﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Banco.Models;

namespace Banco.ViewModels
{
    public class PersonaIndexData
    {
        public IEnumerable<Persona> Personas { get; set; }
        public IEnumerable<Prestamo> Prestamos { get; set; }
        public IEnumerable<Credito> Creditos { get; set; }
        public IEnumerable<Cuenta> Cuentas { get; set; }
        public IEnumerable<Debito> Debitos { get; set; }
        public IEnumerable<Transferencia> Transferencias { get; set; }
        public IEnumerable<Movimiento> Movimientos { get; set; }


    
    }
}