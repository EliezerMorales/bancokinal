namespace Banco.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Banco.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Banco.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Banco.Models.ApplicationDbContext context)
        {
            if (!context.Users.Any(u => u.UserName == "admin@sistemabancario.org"))
            {
                var roleStrore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStrore);

                var userStrore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStrore);
                var user = new ApplicationUser { UserName = "admin@sistemabancario.org", Email = "admin@sistemabancario.org" };

                userManager.Create(user, "Guate$123");

                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });

                userManager.AddToRole(user.Id, "Admin");
                //  This method will be called after migrating to the latest version.

                //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
                //  to avoid creating duplicate seed data. E.g.
                //
                //    context.People.AddOrUpdate(
                //      p => p.FullName,
                //      new Person { FullName = "Andrew Peters" },
                //      new Person { FullName = "Brice Lambson" },
                //      new Person { FullName = "Rowan Miller" }
                //    );
                //
            }
        }
    }
}