﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class CreditoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Creditoes
        public ActionResult Index()
        {
            var creditos = db.Creditos.Include(c => c.Cuenta).Include(c => c.Persona).Include(c => c.TipoTarjeta);
            return View(creditos.ToList());
        }
        [Authorize(Roles = "Admin, User")]
        // GET: Creditoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credito credito = db.Creditos.Find(id);
            if (credito == null)
            {
                return HttpNotFound();
            }
            return View(credito);
        }
        [Authorize(Roles="Admin")]
        // GET: Creditoes/Create
        public ActionResult Create()
        {
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta");
            ViewBag.PersonaId = new SelectList(db.Personas, "Id", "Nombre");
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo");
            return View();
        }

        // POST: Creditoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NumeroTarjeta,Pin,Saldo,Activacion,Vencimiento,Estado,TipoTarjetaId,PersonaId,CuentaId")] Credito credito)
        {
            if (ModelState.IsValid)
            {
                db.Creditos.Add(credito);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", credito.CuentaId);
            ViewBag.PersonaId = new SelectList(db.Personas, "Id", "Nombre", credito.PersonaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", credito.TipoTarjetaId);
            return View(credito);
        }
        [Authorize(Roles = "Admin")]
        // GET: Creditoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credito credito = db.Creditos.Find(id);
            if (credito == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", credito.CuentaId);
            ViewBag.PersonaId = new SelectList(db.Personas, "Id", "Nombre", credito.PersonaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", credito.TipoTarjetaId);
            return View(credito);
        }

        // POST: Creditoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NumeroTarjeta,Pin,Saldo,Activacion,Vencimiento,Estado,TipoTarjetaId,PersonaId,CuentaId")] Credito credito)
        {
            if (ModelState.IsValid)
            {
                db.Entry(credito).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", credito.CuentaId);
            ViewBag.PersonaId = new SelectList(db.Personas, "Id", "Nombre", credito.PersonaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", credito.TipoTarjetaId);
            return View(credito);
        }
        [Authorize(Roles = "Admin")]
        // GET: Creditoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Credito credito = db.Creditos.Find(id);
            if (credito == null)
            {
                return HttpNotFound();
            }
            return View(credito);
        }
        [Authorize(Roles = "Admin")]
        // POST: Creditoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Credito credito = db.Creditos.Find(id);
            db.Creditos.Remove(credito);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
