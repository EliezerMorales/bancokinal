﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banco.Models
{
    public class Prestamo
    {
        public int Id { get; set; }
        public int Meses { get; set; }
        public decimal Monto { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaLimite { get; set; }

        public int PersonaId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual Persona Persona { get; set; }

    }
}