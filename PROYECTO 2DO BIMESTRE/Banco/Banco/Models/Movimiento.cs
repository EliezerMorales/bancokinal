﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banco.Models
{
    public class Movimiento
    {
        public int Id { get; set; }
        public string Lugar { get; set; }
        public DateTime FechaYHora { get; set; }
        public decimal Monto { get; set; }

        public int TipoMovimientoId { get; set; }
        public int CuentaId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual TipoMovimiento TipoMovimiento { get; set; }
        public virtual Cuenta Cuenta { get; set; }
    }
}