﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banco.Models
{
    public class Cuenta
    {
        
        [Display(Name = "Numero")]
        public int Id { get; set; }
        public string NumeroCuenta { get; set; }
        public decimal Saldo { get; set; }
        public bool Estado { get; set; }

        public int PersonaId { get; set; }
        public int TipoCuentaId { get; set; }

        public virtual Persona Persona { get; set; }
        public virtual TipoCuenta TipoCuenta { get; set; }

        public virtual ICollection<Movimiento> Movimientos { get; set; }
        public virtual ICollection<Transferencia> Transferencias { get; set; }

    }
}