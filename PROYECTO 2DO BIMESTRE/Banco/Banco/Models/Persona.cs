﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class Persona
    {
        [Required]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        [RegularExpression(@"[a-zA-ZñÑ\s]{2,50}")]
        [StringLength(20, ErrorMessage = "El nombre no puede llevar mas de 20 caracteres")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "Apellido")]
        [StringLength(20, ErrorMessage = "El apellido no puede llevar mas de 20 caracteres")]
        [RegularExpression(@"[a-zA-ZñÑ\s]{2,50}")]
        public string Apellido { get; set; }

        [Required]
        [Display(Name = "DPI")]
        [RegularExpression(@"[0-9]{1,9}(\.[0-9]{0,2})?$")]
        public string Dpi { get; set; }

        [Required]
        [Display(Name = "Direccion")]
        [StringLength(65, ErrorMessage = "La direccion no puede llevar mas de 65 caracteres")]
        public string Direccion { get; set; }

        [Required]
        [Display(Name = "Teléfono")]
        [RegularExpression(@"[0-9]{1,9}(\.[0-9]{0,2})?$")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [RegularExpression(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$")]
        public string Correo { get; set; }

        [Required]
        [Display(Name = "Nombre Referencia 1 ")]
        [RegularExpression(@"[a-zA-ZñÑ\s]{2,50}")]
        [StringLength(20, ErrorMessage = "El nombre no puede llevar mas de 20 caracteres")]
        public string NombreReferencia { get; set; }

        [Required]
        [Display(Name = "Numero Referencia 1")]
        [RegularExpression(@"[0-9]{1,9}(\.[0-9]{0,2})?$")]
        public string NumeroReferencia { get; set; }

        [Required]
        [Display(Name = "Nombre Referencia 2")]
        [RegularExpression(@"[a-zA-ZñÑ\s]{2,50}")]
        [StringLength(20, ErrorMessage = "El nombre no puede llevar mas de 20 caracteres")]
        public string NombreReferencia1 { get; set; }

        [Required]
        [Display(Name = "Numero Referencia 2")]
        [RegularExpression(@"[0-9]{1,9}(\.[0-9]{0,2})?$")]
        public string NumeroReferencia1 { get; set; }


        public virtual ICollection<Cuenta> Cuentas { get; set; }
        public virtual ICollection<Prestamo> Prestamos { get; set; }
        public virtual ICollection<Credito> Creditos { get; set; }
   
    }
}